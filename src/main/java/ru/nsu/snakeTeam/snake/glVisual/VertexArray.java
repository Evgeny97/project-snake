package ru.nsu.snakeTeam.snake.glVisual;


import ru.nsu.snakeTeam.snake.utils.Utilities;

import static org.lwjgl.opengl.GL30.*;

public class VertexArray {

    private int vaoId;
    private int vertCount;

    public static final int VERTEX_ATTRIBUTE = 0;
    public static final int COORD_ATTRIBUTE = 1;
    private int ibo;

    public VertexArray(float[] vertices, byte[] indices, float[] textureCoords) {
        vertCount = indices.length;
        createArrayObject(vertices, indices,textureCoords);
    }

    public void createArrayObject(float[] vertices, byte[] indices,float[] textureCoords) {
        vaoId = glGenVertexArrays();
        glBindVertexArray(vaoId);
        createVerticesBuffer(vertices);
        createIndicesBuffer(indices);
        createTextureCoordsBuffer(textureCoords);
        glBindVertexArray(0);
    }

    private void createVerticesBuffer(float[] vertices) {
        int vboId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        glBufferData(GL_ARRAY_BUFFER, Utilities.createFloatBuffer(vertices), GL_STATIC_DRAW);
        glVertexAttribPointer(VERTEX_ATTRIBUTE, 3, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(VERTEX_ATTRIBUTE);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    private void createTextureCoordsBuffer(float[] textureCoords) {
        int tboId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, tboId);
        glBufferData(GL_ARRAY_BUFFER, Utilities.createFloatBuffer(textureCoords), GL_STATIC_DRAW);
        glVertexAttribPointer(COORD_ATTRIBUTE, 2, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(COORD_ATTRIBUTE);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    private void createIndicesBuffer(byte[] indices) {
        ibo = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, Utilities.createByteBuffer(indices), GL_STATIC_DRAW);
    }

    public void draw(){
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBindVertexArray(vaoId);
        glDrawElements(GL_TRIANGLES,vertCount,GL_UNSIGNED_BYTE,0);
        //glDrawArrays(GL_TRIANGLES,0,3);
        glBindVertexArray(0);
    }


}