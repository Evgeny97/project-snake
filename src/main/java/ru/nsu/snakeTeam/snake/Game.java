package ru.nsu.snakeTeam.snake;

import ru.nsu.snakeTeam.snake.gameView.LevelView;

public class Game {
    private static Game INSTANCE;
    private LevelView levelView;
    private Driver driver;
    private boolean running = true;
    private boolean exit = false;
    private boolean needRestart = false;

    private Game() {
    }

    public static synchronized Game getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Game();
        }
        return INSTANCE;
    }

    public void init() {
        levelView = new LevelView();
        driver = new Driver(levelView);
        levelView.setMouseCallback(driver.getMouseClickCallback());
        levelView.initGraphics();
    }

    public void restart() {
        needRestart = true;
        running = false;
        levelView = new LevelView();
        driver.setLevelView(levelView);
        levelView.setMouseCallback(driver.getMouseClickCallback());
        levelView.initGraphics();
    }

    public void exit() {
        running = false;
        exit = true;
    }


    public Driver getDriver() {
        return driver;
    }

    public void start() {
        init();
        do {
            needRestart = false;
            running = true;
            exit = false;

            timeLoop();
            if (exit || driver.shouldClose()) {
                driver.close();
            }
        }
        while (needRestart);
    }

    private void timeLoop() {
        long lastTime = System.nanoTime();
        double delta = 0.0;
        double ns = 1_000_000_000.0;
        long timer = System.currentTimeMillis();
        double updTime = 0.25;
        int updates = 0;
        int frames = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= updTime) {
                driver.update();
                updates++;
                delta -= updTime;
                if (!running) {
                    break;
                }
            }
            driver.render();

            frames++;
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println(updates + " ups, " + frames + " fps");
                updates = 0;
                frames = 0;

            }

            if (driver.shouldClose()) {
                running = false;
            }
        }
    }
}
