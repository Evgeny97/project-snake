package ru.nsu.snakeTeam.snake.gameModel;

import java.util.Objects;

public class LevelPosition {
    public int x;
    public int y;

    public LevelPosition() {
        x = 0;
        y = 0;
    }

    public LevelPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public LevelPosition clone(){
        return new LevelPosition(x,y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LevelPosition that = (LevelPosition) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
