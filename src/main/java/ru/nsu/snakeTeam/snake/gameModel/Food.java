package ru.nsu.snakeTeam.snake.gameModel;

public class Food {
    public LevelPosition getPosition() {
        return position;
    }

    public Food() {
        this.position = new LevelPosition();
    }

    public void setPosition(LevelPosition position) {
        this.position = position;
    }

    private LevelPosition position;
}
