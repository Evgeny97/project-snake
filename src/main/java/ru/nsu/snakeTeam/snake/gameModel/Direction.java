package ru.nsu.snakeTeam.snake.gameModel;

public enum Direction {
    UP, RIGHT, DOWN, LEFT;

    public Direction getOpposite() {
        switch (this) {
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            case DOWN:
                return UP;
            default:
                return DOWN;
        }
    }

    public boolean isOpposite(Direction direction) {
        return direction == getOpposite();
    }

}
