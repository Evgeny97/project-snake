package ru.nsu.snakeTeam.snake.gameModel;


import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Random;

public class Level {
    private final static int step = 1;
    private final int width = 15;
    private final int height = 15;
    private ArrayDeque<BodyPart> bodyParts = new ArrayDeque<>();
    private Food food = new Food();
    private Random random = new Random();
    private boolean isAte = false;
    private Direction movementDirection = Direction.UP;
    private boolean dead = false;

    public Level() {
        bodyParts.add(new BodyPart(0, 3, Direction.UP));
        bodyParts.add(new BodyPart(0, 2, Direction.UP));
        bodyParts.add(new BodyPart(0, 1, Direction.UP));
        bodyParts.add(new BodyPart(0, 0, Direction.UP));
        spawnNewFood();
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isAte() {
        return isAte;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ArrayDeque<BodyPart> getBodyParts() {
        return bodyParts.clone();
    }

    public void move(Deque<Direction> lastDirections) {
        if (!dead) {
            for (Direction dir : lastDirections) {
                if (dir.isOpposite(movementDirection)) {
                    continue;
                }
                movementDirection = dir;
                break;
            }
            BodyPart lastHead = bodyParts.getFirst();
            LevelPosition newHeadPos = lastHead.getPosition().clone();
            switch (movementDirection) {
                case UP:
                    newHeadPos.y += step;
                    break;
                case DOWN:
                    newHeadPos.y -= step;
                    break;
                case RIGHT:
                    newHeadPos.x += step;
                    break;
                case LEFT:
                    newHeadPos.x -= step;
                    break;

            }
            if (isDead(newHeadPos)) {
                dead = true;
                return;
            }
            bodyParts.addFirst(new BodyPart(newHeadPos, movementDirection));

            if (!newHeadPos.equals(food.getPosition())) {
                isAte = false;
                bodyParts.removeLast();
            } else {
                isAte = true;
                spawnNewFood();
            }
        }
    }

    private boolean isDead(LevelPosition headPos) {
        boolean alive = !isBodyCollision(headPos);
        if (alive && (headPos.x < 0 || headPos.x > width - 1 || headPos.y < 0 || headPos.y > height - 1)) {
            alive = false;
        }
        return !alive;
    }

    private void spawnNewFood() {
        LevelPosition newPos;
        do {
            newPos = new LevelPosition(random.nextInt(width), random.nextInt(height));
        } while (!isPosEmpty(newPos));
        food.setPosition(newPos);
    }

    private boolean isPosEmpty(LevelPosition pos) {
        boolean empty = !isBodyCollision(pos);
        if (empty && food.getPosition().equals(pos)) {
            empty = false;
        }
        return empty;
    }

    private boolean isBodyCollision(LevelPosition pos) {
        boolean empty = false;
        for (BodyPart bp : bodyParts) {
            if (bp.getPosition().equals(pos)) {
                empty = true;
                break;
            }
        }
        return empty;
    }


    public LevelPosition getFoodPos() {
        return food.getPosition();
    }
}
