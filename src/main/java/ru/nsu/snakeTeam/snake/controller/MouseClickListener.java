package ru.nsu.snakeTeam.snake.controller;

import ru.nsu.snakeTeam.snake.gameView.ViewPosition;

public interface MouseClickListener {
    void action(ViewPosition clickPosition);
}
