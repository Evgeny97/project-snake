package ru.nsu.snakeTeam.snake.controller;

import org.lwjgl.glfw.GLFWCursorPosCallback;

public class MousePosCallback extends GLFWCursorPosCallback {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public void invoke(long window, double xpos, double ypos) {
        x = xpos;
        y = ypos;
    }
}
