package ru.nsu.snakeTeam.snake.controller;

import org.lwjgl.glfw.GLFWMouseButtonCallback;
import ru.nsu.snakeTeam.snake.Driver;
import ru.nsu.snakeTeam.snake.Game;

import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.*;

public class MouseClickCallback extends GLFWMouseButtonCallback {
    private MousePosCallback posCallback;
    private ArrayList<MouseClickListener> listeners = new ArrayList<>();
    private boolean realised = true;

    public MouseClickCallback(MousePosCallback pos) {
        posCallback = pos;
    }

    public void addListener(MouseClickListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MouseClickListener listener){
        listeners.remove(listener);
    }

    @Override
    public void invoke(long window, int button, int action, int mods) {
        float x = (float) posCallback.getX();
        float y = (float) posCallback.getY();
        if (realised && button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
            for (MouseClickListener listener : listeners) {
                listener.action(Game.getInstance().getDriver().fromScreenToViewCoords(x,y));
            }
            realised = false;
        }
        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
            realised = true;
        }
    }
}
