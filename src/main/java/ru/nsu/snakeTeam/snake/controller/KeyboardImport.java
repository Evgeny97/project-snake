package ru.nsu.snakeTeam.snake.controller;

import org.lwjgl.glfw.GLFWKeyCallback;

import java.util.ArrayDeque;

import static org.lwjgl.glfw.GLFW.*;


public class KeyboardImport extends GLFWKeyCallback {

    private ArrayDeque<Integer> lastDirectionButtons = new ArrayDeque<>(2);

    public ArrayDeque<Integer> getLastDirectionButtons() {
        ArrayDeque<Integer> copy = lastDirectionButtons.clone();
        lastDirectionButtons.clear();
        return copy;
    }

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            if (isDirectionButton(key)) {
                lastDirectionButtons.addFirst(key);
                if (lastDirectionButtons.size() > 2) {
                    lastDirectionButtons.removeLast();
                }
            }
        }
    }

    private boolean isDirectionButton(int key) {
        return key == GLFW_KEY_UP || key == GLFW_KEY_DOWN || key == GLFW_KEY_LEFT || key == GLFW_KEY_RIGHT;
    }
}
