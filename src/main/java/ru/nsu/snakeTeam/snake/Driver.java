package ru.nsu.snakeTeam.snake;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import ru.nsu.snakeTeam.snake.controller.KeyboardImport;
import ru.nsu.snakeTeam.snake.controller.MouseClickCallback;
import ru.nsu.snakeTeam.snake.controller.MousePosCallback;
import ru.nsu.snakeTeam.snake.gameModel.Direction;
import ru.nsu.snakeTeam.snake.gameView.LevelView;
import ru.nsu.snakeTeam.snake.gameView.ViewPosition;

import java.util.ArrayDeque;
import java.util.Deque;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.system.MemoryUtil.NULL;


public class Driver {
    private static float scale = 6;
    private LevelView levelView;
    private GLFWErrorCallback errorCallback;
    private long window;
    private KeyboardImport keyCallback;
    private MousePosCallback mousePosCallback;
    private MouseClickCallback mouseClickCallback;
    private int width = 800;
    private int height = 800;
    public Driver(LevelView levelView) {
        this.levelView = levelView;
        init(levelView);
    }

    public void setLevelView(LevelView levelView) {
        this.levelView = levelView;
    }

    public MouseClickCallback getMouseClickCallback() {
        return mouseClickCallback;
    }

    private void init(LevelView levelView) {
        errorCallback = GLFWErrorCallback.createPrint(System.err);
        glfwSetErrorCallback(errorCallback);

        if (!glfwInit()) {
            System.err.println("init error");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        width = (int) (levelView.getWidth() * scale);
        height = (int) (levelView.getHeight() * scale);

        window = glfwCreateWindow(width, height, "Snake", NULL, NULL);

        if (window == NULL) {
            System.err.println("window create error");
        }

        keyCallback = new KeyboardImport();
        glfwSetKeyCallback(window, keyCallback);
        mousePosCallback = new MousePosCallback();
        glfwSetCursorPosCallback(window, mousePosCallback);
        mouseClickCallback = new MouseClickCallback(mousePosCallback);
        glfwSetMouseButtonCallback(window, mouseClickCallback);

        GLFWVidMode videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        glfwSetWindowPos(window, (videoMode != null ? (videoMode.width() - width) / 2 : 0),
                (videoMode != null ? (videoMode.height() - height) / 2 : 0));

        glfwMakeContextCurrent(window);
        GL.createCapabilities();

        glfwShowWindow(window);
        glfwSwapInterval(0);

        System.out.println(glGetString(GL_VERSION));

        glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
        glEnable(GL_DEPTH_TEST);
    }

    public ViewPosition fromScreenToViewCoords(float x, float y) {
        ViewPosition position = new ViewPosition((x - width / 2.0f) / scale,
                -(y - height / 2.0f) / scale);
        return position;
    }

    void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        levelView.render();
        glfwSwapBuffers(window);
    }

    void update() {
        glfwPollEvents();
        levelView.updateModel(transformToDirections(keyCallback.getLastDirectionButtons()));
    }

    private Deque<Direction> transformToDirections(Deque<Integer> buttons) {
        Deque<Direction> directions = new ArrayDeque<>();
        for (int key : buttons) {
            switch (key) {
                case GLFW_KEY_DOWN:
                    directions.addLast(Direction.DOWN);
                    break;
                case GLFW_KEY_LEFT:
                    directions.addLast(Direction.LEFT);
                    break;
                case GLFW_KEY_RIGHT:
                    directions.addLast(Direction.RIGHT);
                    break;
                default:
                    directions.addLast(Direction.UP);
                    break;
            }
        }
        return directions;
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(window);
    }


    public void close() {
        glfwDestroyWindow(window);
//        glfwTerminate();
    }


}
