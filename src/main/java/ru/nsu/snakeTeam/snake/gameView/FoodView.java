package ru.nsu.snakeTeam.snake.gameView;

import ru.nsu.snakeTeam.snake.glVisual.Shader;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.glVisual.VertexArray;
import ru.nsu.snakeTeam.snake.math.Matrix4f;
import ru.nsu.snakeTeam.snake.math.Vector3f;

public class FoodView implements GameObjectView {

    private static float size = 5;
    private static float[] textureCoords = new float[]{
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f};
    private static byte[] indices = new byte[]{
            0, 1, 2,
            0, 2, 3};
    private static Texture texture = new Texture("textures/apple.png");
    private static Shader shader = new Shader("shaders/body.vert", "shaders/body.frag");
    private float[] vertices;
    private Matrix4f pr_matrix;
    private VertexArray vao;
    private Matrix4f modelMatrix;

    public FoodView(ViewPosition position, Matrix4f pr_matrix, float size) {
        FoodView.size = size;
        vertices = new float[]{
                -1.0f * size / 2, 1.0f * size / 2, 0,
                -1.0f * size / 2, -1.0f * size / 2, 0,
                1.0f * size / 2, -1.0f * size / 2, 0,
                1.0f * size / 2, 1.0f * size / 2, 0};

        vao = new VertexArray(vertices, indices, textureCoords);

        this.pr_matrix = pr_matrix;
        shader.enable();
        shader.setUniformMat4f("view_matrix", this.pr_matrix);
        shader.disable();
        update(position);
    }

    public static float getSize() {
        return size;
    }


    public void update(ViewPosition position) {
        Matrix4f translate = Matrix4f.translate(new Vector3f(position.x, position.y, 0.0f));
        modelMatrix = translate;
    }

    public void render() {
        texture.bind();
        shader.enable();
        shader.setUniformMat4f("model_matrix", modelMatrix);
        vao.draw();
        shader.disable();
        texture.unbind();
    }

}
