package ru.nsu.snakeTeam.snake.gameView.gui;

import java.util.ArrayList;

public class BaseGuiElement {
    protected static float viewLevelAdjustment = 0.01f;
    protected ArrayList<BaseGuiElement> children = new ArrayList<>();
    protected float viewLevel = 0.5f;
    private boolean visible = true;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        for (BaseGuiElement child : children) {
            child.setVisible(visible);
        }
    }

    public void addChild(BaseGuiElement child) {
        child.viewLevel += viewLevelAdjustment;
        children.add(child);
    }

    public void removeChild(BaseGuiElement child) {
        children.remove(child);
    }

    public void render() {
        renderChildren();
    }

    protected void renderChildren() {
        for (BaseGuiElement child : children) {
            child.render();
        }
    }

      public void destroy(){
          destroyAllChildren();
      }

    protected void destroyAllChildren() {
        for (BaseGuiElement child : children) {
            child.destroy();
        }
    }
}
