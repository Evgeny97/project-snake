package ru.nsu.snakeTeam.snake.gameView;

public class ViewPosition {
    public float x;
    public float y;

    public ViewPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public ViewPosition() {
    }
}
