package ru.nsu.snakeTeam.snake.gameView;

import ru.nsu.snakeTeam.snake.gameModel.Direction;
import ru.nsu.snakeTeam.snake.glVisual.Shader;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.glVisual.VertexArray;

import ru.nsu.snakeTeam.snake.math.Matrix4f;
import ru.nsu.snakeTeam.snake.math.Vector3f;

import java.util.HashMap;
import java.util.Map;

public class BodyPartView implements GameObjectView {

    private static Map<BodyType, Texture> textureMap = null;

    public static float getSize() {
        return size;
    }

    private static boolean initiated = false;

    private final static float size = 5;

    private Matrix4f pr_matrix;

    private static float[] vertices = new float[]{
            -1.0f * size / 2, 1.0f * size / 2, 0,
            -1.0f * size / 2, -1.0f * size / 2, 0,
            1.0f * size / 2, -1.0f * size / 2, 0,
            1.0f * size / 2, 1.0f * size / 2, 0};

    private static float[] textureCoords = new float[]{
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f};

    private static byte[] indices = new byte[]{
            0, 1, 2,
            0, 2, 3};

    private Texture texture;

    private VertexArray vao = new VertexArray(vertices, indices, textureCoords);

    private BodyType bodyType = BodyType.STRAIGHT;


    private static Shader shader;
    private Matrix4f modelMatrix;


    public BodyPartView(BodyType bodyType, Direction direction, Direction lastPartDirection,
                        ViewPosition position, Matrix4f pr_matrix) {
        this.pr_matrix = pr_matrix;
        if (!initiated) {
            init(pr_matrix);
        }
        update(bodyType, direction, lastPartDirection, position);
    }

    private static synchronized void init(Matrix4f pr_matrix) {
        initiated = true;
        shader = new Shader("shaders/body.vert", "shaders/body.frag");
        shader.enable();
        shader.setUniformMat4f("view_matrix", pr_matrix);
        shader.disable();
        textureMap = new HashMap<>();
        textureMap.put(BodyType.HEAD, new Texture("textures/snake-head.png"));
        textureMap.put(BodyType.CORNER, new Texture("textures/snake-body-corner.png"));
        textureMap.put(BodyType.STRAIGHT, new Texture("textures/snake-body.png"));
        textureMap.put(BodyType.TAIL, new Texture("textures/snake-tail.png"));


    }

    public void update(BodyType bodyType, Direction direction, Direction lastPartDirection,
                       ViewPosition position) {
        setTexture(bodyType, direction, lastPartDirection);
        Matrix4f rotationMatrix = getRotateMatrix(bodyType, direction, lastPartDirection);
        Matrix4f translate = Matrix4f.translate(new Vector3f(position.x , position.y, 0.0f));

        modelMatrix = translate.multiply(rotationMatrix);
    }



    private void setTexture(BodyType bodyType, Direction direction, Direction lastPartDirection) {
        texture = textureMap.get(bodyType);
    }

    private Matrix4f getRotateMatrix(BodyType bodyType, Direction direction, Direction lastPartDirection) {
        Matrix4f rotateMatrix;
        if (bodyType == BodyType.CORNER) {
            if ((direction == Direction.UP && lastPartDirection == Direction.LEFT)
                    || (direction == Direction.RIGHT && lastPartDirection == Direction.DOWN)) {
                rotateMatrix = Matrix4f.rotate(0);
            } else if ((direction == Direction.RIGHT && lastPartDirection == Direction.UP)
                    || (direction == Direction.DOWN && lastPartDirection == Direction.LEFT)) {
                rotateMatrix = Matrix4f.rotate(270);
            } else if ((direction == Direction.DOWN && lastPartDirection == Direction.RIGHT)
                    || (direction == Direction.LEFT && lastPartDirection == Direction.UP)) {
                rotateMatrix = Matrix4f.rotate(180);
            } else {
                rotateMatrix = Matrix4f.rotate(90);
            }
        } else {
            switch ((bodyType == BodyType.HEAD) ? direction : lastPartDirection) {
                case LEFT:
                    rotateMatrix = Matrix4f.rotate(90);
                    break;
                case DOWN:
                    rotateMatrix = Matrix4f.rotate(180);
                    break;
                case RIGHT:
                    rotateMatrix = Matrix4f.rotate(270);
                    break;
                default:
                    rotateMatrix = Matrix4f.rotate(0);
                    break;
            }
        }

        return rotateMatrix;
    }

    public void render() {
        texture.bind();
        shader.enable();
        shader.setUniformMat4f("model_matrix", modelMatrix);
        vao.draw();
        shader.disable();
        texture.unbind();
    }

}