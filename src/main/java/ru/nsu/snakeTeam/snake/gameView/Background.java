package ru.nsu.snakeTeam.snake.gameView;


import ru.nsu.snakeTeam.snake.glVisual.Shader;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.glVisual.VertexArray;
import ru.nsu.snakeTeam.snake.math.Matrix4f;

public class Background implements GameObjectView {

    private static byte[] indices = new byte[]{
            0, 1, 2,
            0, 2, 3};
    private static Texture texture = new Texture("textures/grass.jpg");
    private static Shader shader = new Shader("shaders/background.vert", "shaders/background.frag");
    private float[] vertices;
    private float[] textureCoords;
    private Matrix4f pr_matrix;
    private VertexArray vao;

    public Background(float width, float height, Matrix4f pr_matrix) {
        vertices = new float[]{
                -width / 2.0f, height / 2.0f, -0.5f,
                -width / 2.0f, -height / 2.0f, -0.5f,
                width / 2.0f, -height / 2.0f, -0.5f,
                width / 2.0f, height / 2.0f, -0.5f};

        textureCoords = new float[]{
                0.0f, height * 0.05f,
                0.0f, 0.0f,
                width * 0.05f, 0.0f,
                width * 0.05f, height * 0.05f};

        vao = new VertexArray(vertices, indices, textureCoords);
        shader.enable();
        this.pr_matrix = pr_matrix;
        shader.setUniformMat4f("pr_matrix", this.pr_matrix);
        shader.disable();
    }

    public void render() {
        texture.bind();
        shader.enable();
        vao.draw();
        shader.disable();
        texture.unbind();
    }

}
