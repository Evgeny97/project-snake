package ru.nsu.snakeTeam.snake.gameView;

import ru.nsu.snakeTeam.snake.Game;
import ru.nsu.snakeTeam.snake.controller.MouseClickCallback;
import ru.nsu.snakeTeam.snake.gameModel.BodyPart;
import ru.nsu.snakeTeam.snake.gameModel.Direction;
import ru.nsu.snakeTeam.snake.gameModel.Level;
import ru.nsu.snakeTeam.snake.gameModel.LevelPosition;
import ru.nsu.snakeTeam.snake.gameView.gui.BaseGuiElement;
import ru.nsu.snakeTeam.snake.gameView.gui.RectangleButton;
import ru.nsu.snakeTeam.snake.gameView.gui.VisibleGuiElement;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.math.Matrix4f;

import java.util.ArrayList;
import java.util.Deque;

public class LevelView implements GameObjectView {
    private Matrix4f pr_matrix;
    private final float width;
    private final float height;
    private Background background;
    private ArrayList<GameObjectView> snakeBody = new ArrayList<>();
    private FoodView foodView;
    private Level level = new Level();
    private float itemSize = 5;
    private boolean firstUpdate = true;
    private BaseGuiElement rootGui = new BaseGuiElement();
    private MouseClickCallback mouseCallback;
    private boolean gameOver = false;

    public LevelView() {
        width = level.getWidth() * BodyPartView.getSize();
        height = level.getHeight() * BodyPartView.getSize();
        pr_matrix = Matrix4f.orthographic(-width / 2.0f, width / 2.0f,
                -height / 2.0f, height / 2.0f, -1.0f, 1.0f);

    }

    public void setMouseCallback(MouseClickCallback mouseCallback) {
        this.mouseCallback = mouseCallback;
    }

    public void initGraphics() {
        background = new Background(width, height, pr_matrix);
        foodView = new FoodView(levelToViewTransform(level.getFoodPos()), pr_matrix, itemSize);
    }

    public void updateModel(Deque<Direction> lastDirections) {
        level.move(lastDirections);
        updateBodyView();
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getLevelWidth() {
        return level.getWidth();
    }

    public float getLevelHeight() {
        return level.getHeight();
    }

    private void updateBodyView() {
        ArrayList<BodyPart> bodyParts = new ArrayList<>(level.getBodyParts());

        snakeBody.clear();
        BodyType type;
        Direction direction = null;
        Direction lastDirection;

        for (int i = 0; i < bodyParts.size(); i++) {
            BodyPart bodyPart = bodyParts.get(i);
            lastDirection = direction;
            direction = bodyPart.getDirection();
            if (i == 0) {
                type = BodyType.HEAD;
            } else if (i == bodyParts.size() - 1) {
                type = BodyType.TAIL;
            } else if (direction == lastDirection) {
                type = BodyType.STRAIGHT;
            } else {
                type = BodyType.CORNER;
            }
            snakeBody.add(new BodyPartView(type, direction, lastDirection,
                    levelToViewTransform(bodyPart.getPosition()), pr_matrix));
        }
        if (level.isAte() || firstUpdate) {
            foodView.update(levelToViewTransform(level.getFoodPos()));
        }
        firstUpdate = false;
        if (!gameOver && level.isDead()) {
            gameOver = true;
            showGameOverGui();
        }
    }

    private ViewPosition levelToViewTransform(LevelPosition levelPosition) {
        ViewPosition viewPosition = new ViewPosition();
        viewPosition.x = (levelPosition.x - (level.getWidth() / 2.0f) + 0.5f) * BodyPartView.getSize();
        viewPosition.y = (levelPosition.y - (level.getHeight() / 2.0f) + 0.5f) * BodyPartView.getSize();
        return viewPosition;
    }

    private void showGameOverGui() {
        float[] vertices = new float[]{
                -width / 2.5f, height / 2.5f * 3 / 4, -0.1f,
                -width / 2.5f, -height / 2.5f * 3 / 4, -0.1f,
                width / 2.5f, -height / 2.5f * 3 / 4, -0.1f,
                width / 2.5f, height / 2.5f * 3 / 4, -0.1f};
        byte[] indices = new byte[]{
                0, 1, 2,
                0, 2, 3};
        float[] textureCoords = new float[]{
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f};

        VisibleGuiElement deathWindow = new VisibleGuiElement(new Texture("textures/gui/GameOverMenu.png"),
                vertices, indices, textureCoords, pr_matrix);
//        deathWindow.setVisible(true);

        RectangleButton yesButton = new RectangleButton(mouseCallback, (pos) -> Game.getInstance().restart(),
                new Texture("textures/gui/yes.png"), width / 4f, height / 9,
                -width / 4.5f, -height / 5, pr_matrix);
        deathWindow.addChild(yesButton);

        RectangleButton noButton = new RectangleButton(mouseCallback, (pos) -> Game.getInstance().exit(),
                new Texture("textures/gui/no.png"), width / 4f, height / 9,
                width / 4.5f, -height / 5, pr_matrix);
        deathWindow.addChild(noButton);

        rootGui.addChild(deathWindow);
    }


    @Override
    public void render() {
        background.render();
        for (GameObjectView bodyPart : snakeBody) {
            bodyPart.render();
        }
        foodView.render();
        rootGui.render();
    }
}
