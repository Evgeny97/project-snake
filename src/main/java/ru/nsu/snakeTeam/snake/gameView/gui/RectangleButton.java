package ru.nsu.snakeTeam.snake.gameView.gui;

import ru.nsu.snakeTeam.snake.controller.MouseClickCallback;
import ru.nsu.snakeTeam.snake.controller.MouseClickListener;
import ru.nsu.snakeTeam.snake.gameView.ViewPosition;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.math.Matrix4f;
import ru.nsu.snakeTeam.snake.math.Vector3f;

public class RectangleButton extends BaseButton {
    private float width;
    private float height;
    private float posX;
    private float posY;

    public RectangleButton(MouseClickCallback mouseCallback, MouseClickListener action, Texture texture, float width,
                           float height, float posX, float posY, Matrix4f pr_matrix) {
        this.width = width;
        this.height = height;
        this.posX = posX;
        this.posY = posY;
        float[] vertices = new float[]{
                -width / 2.0f, height / 2.0f, -0.9f,
                -width / 2.0f, -height / 2.0f, -0.9f,
                width / 2.0f, -height / 2.0f, -0.9f,
                width / 2.0f, height / 2.0f, -0.9f};
        byte[] indices = new byte[]{
                0, 1, 2,
                0, 2, 3};
        float[] textureCoords = new float[]{
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f};
        buttonShader.enable();
        Matrix4f translate = Matrix4f.translate(new Vector3f(posX, posY, 0f));
        buttonShader.setUniformMat4f("model_matrix", translate);
        buttonShader.disable();
        super.init(mouseCallback, action, texture, vertices, indices, textureCoords, pr_matrix);
    }


    @Override
    boolean isTarget(ViewPosition clickPosition) {
        return !(clickPosition.x > posX + width / 2.0f || clickPosition.x < posX - width / 2.0f
                || clickPosition.y > posY + height / 2.0f || clickPosition.y < posY - height / 2.0f);
    }
}
