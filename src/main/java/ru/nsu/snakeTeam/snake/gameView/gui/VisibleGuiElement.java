package ru.nsu.snakeTeam.snake.gameView.gui;

import ru.nsu.snakeTeam.snake.glVisual.Shader;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.glVisual.VertexArray;
import ru.nsu.snakeTeam.snake.math.Matrix4f;

public class VisibleGuiElement extends BaseGuiElement {
    private static Shader shader = new Shader("shaders/gui.vert", "shaders/gui.frag");

    protected Texture texture;
    protected float[] vertices;
    protected byte[] indices;
    protected float[] textureCoords;
    protected Matrix4f pr_matrix;
    protected VertexArray vao;

    protected VisibleGuiElement(){}

    public VisibleGuiElement(Texture texture, float[] vertices, byte[] indices, float[] textureCoords, Matrix4f pr_matrix) {
        init(texture, vertices, indices, textureCoords, pr_matrix);
    }

    protected void init(Texture texture, float[] vertices, byte[] indices, float[] textureCoords, Matrix4f pr_matrix) {
        this.texture = texture;
        this.vertices = vertices;
        this.indices = indices;
        this.textureCoords = textureCoords;
        this.pr_matrix = pr_matrix;
        setViewLevel();
        vao = new VertexArray(this.vertices, this.indices, this.textureCoords);
        shader.enable();
        shader.setUniformMat4f("pr_matrix", this.pr_matrix);
        shader.disable();
    }

    private void setViewLevel() {
        int i = 2;
        while (i < vertices.length) {
            vertices[i] = viewLevel;
            i += 3;
        }
    }

    @Override
    public void render() {
        renderChildren();
        if (isVisible()) {
            texture.bind();
            shader.enable();
            vao.draw();
            shader.disable();
            texture.unbind();
        }
    }
}
