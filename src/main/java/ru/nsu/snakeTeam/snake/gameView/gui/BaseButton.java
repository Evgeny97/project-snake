package ru.nsu.snakeTeam.snake.gameView.gui;

import ru.nsu.snakeTeam.snake.controller.MouseClickCallback;
import ru.nsu.snakeTeam.snake.controller.MouseClickListener;
import ru.nsu.snakeTeam.snake.gameView.ViewPosition;
import ru.nsu.snakeTeam.snake.glVisual.Shader;
import ru.nsu.snakeTeam.snake.glVisual.Texture;
import ru.nsu.snakeTeam.snake.math.Matrix4f;

public abstract class BaseButton extends VisibleGuiElement implements MouseClickListener {

    private MouseClickCallback mouseCallback;

    private MouseClickListener mouseClickListener;
    protected Shader buttonShader = new Shader("shaders/button.vert", "shaders/button.frag");

    public BaseButton(MouseClickCallback mouseCallback, MouseClickListener mouseClickListener, Texture texture, float[] vertices, byte[] indices, float[] textureCoords, Matrix4f pr_matrix) {
        init(mouseCallback, mouseClickListener, texture, vertices, indices, textureCoords, pr_matrix);
    }

    protected BaseButton() {
    }

    protected void init(MouseClickCallback mouseCallback, MouseClickListener mouseClickListener, Texture texture, float[] vertices, byte[] indices, float[] textureCoords, Matrix4f pr_matrix) {
        super.init(texture, vertices, indices, textureCoords, pr_matrix);
        this.mouseClickListener = mouseClickListener;
        this.mouseCallback = mouseCallback;
        this.mouseCallback.addListener(this);
        buttonShader.enable();
        buttonShader.setUniformMat4f("view_matrix", pr_matrix);
        buttonShader.disable();
    }
    abstract boolean isTarget(ViewPosition position);

    protected void removeListener(){
        mouseCallback.removeListener(this);
    }

    @Override
    public void render() {
        renderChildren();
        if (isVisible()) {
            texture.bind();
            buttonShader.enable();
            vao.draw();
            buttonShader.disable();
            texture.unbind();
        }

    }

    @Override
    public void action(ViewPosition clickPosition) {
        if(isTarget(clickPosition)){
            mouseClickListener.action(clickPosition);
        }
//        System.out.println(clickPosition.x +" " + clickPosition.y);
    }

    @Override
    public void destroy(){
        removeListener();
        destroyAllChildren();
    }
}
